CREATE DATABASE Biblioteca
GO

USE Biblioteca
GO

CREATE TABLE Estados(
	IdEstado INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(50) NOT NULL,
	Sigla NVARCHAR(2) NOT NULL
		
	CONSTRAINT PK_Estados PRIMARY KEY (IdEstado)
)
GO

CREATE TABLE Cidades(
	IdCidade INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(50) NOT NULL,
	IdEstado INT NOT NULL
		
	CONSTRAINT PK_Cidades PRIMARY KEY (IdCidade),
	CONSTRAINT FK_Cidades_Estados FOREIGN KEY (IdEstado) REFERENCES Estados(IdEstado)
)
GO

CREATE TABLE Enderecos(
	IdEndereco INT NOT NULL IDENTITY(1,1),
	Logadouro NVARCHAR(50) NOT NULL,
	Bairro NVARCHAR(50) NOT NULL,
	Cep NVARCHAR(9) NOT NULL,
	Numero NVARCHAR(10) NOT NULL,
	Complemento NVARCHAR(50),
	IdCidade INT NOT NULL

	CONSTRAINT PK_Endereco PRIMARY KEY (IdEndereco),
	CONSTRAINT FK_Endereco_Cidades FOREIGN KEY (IdCidade) REFERENCES Cidades(IdCidade)
)
GO

CREATE TABLE Pessoas(
	IdPessoa INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(50) NOT NULL,
	Rg NVARCHAR(12) NOT NULL,
	DataNascimento DATE NOT NULL,
	Cpf NVARCHAR(14) NOT NULL,
	Ativo BIT NOT NULL,
	DataCadastro DATE NOT NULL,
	IdEndereco INT NOT NULL
		
	CONSTRAINT PK_Pessooas PRIMARY KEY (IdPessoa),
	CONSTRAINT FK_Pessoas_Endereco FOREIGN KEY (IdEndereco) REFERENCES Enderecos(IdEndereco) 
)
GO

CREATE TABLE TiposTelefone(
	IdTipoTelefone INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(10) NOT NULL
		
	CONSTRAINT PK_TiposTelefone PRIMARY KEY (IdTipoTelefone)
)
GO

CREATE TABLE Telefones(
	IdTelefone INT NOT NULL IDENTITY(1,1),
	Numero NVARCHAR(14) NOT NULL,
	IdTipoTelefone INT NOT NULL,
	DataCadastro DATE NOT NULL,
	Ativo BIT NOT NULL
		
	CONSTRAINT PK_Telefones PRIMARY KEY (IdTelefone),
	CONSTRAINT FK_Telefones_TiposTelefones FOREIGN KEY (IdTipoTelefone) REFERENCES TiposTelefone(IdTipoTelefone) 
)
GO

CREATE TABLE TelefonesPessoas(
	IdTelefonePessoa INT NOT NULL IDENTITY(1,1),
	IdTelefone INT NOT NULL,
	IdPessoa INT NOT NULL,
	DataCadastro DATE NOT NULL
		
	CONSTRAINT PK_TelefonePessoas PRIMARY KEY (IdTelefonePessoa),
	CONSTRAINT FK_TelefonePessoas_Telefone FOREIGN KEY (IdTelefone) REFERENCES Telefones(IdTelefone),
	CONSTRAINT FK_TelefonePessoas_Pessoas FOREIGN KEY (IdPessoa) REFERENCES Pessoas(IdPessoa) 
)
GO

CREATE TABLE Usuarios(
	IdUsuario INT NOT NULL IDENTITY(1,1),
	Email NVARCHAR(100) NOT NULL,
	Senha NVARCHAR(32) NOT NULL,
	DataCadastro DATE NOT NULL,
	Ativo BIT NOT NULL,
	IdPessoa INT NOT NULL
		
	CONSTRAINT PK_Usuarios PRIMARY KEY (IdUsuario),
	CONSTRAINT FK_Usuarios_Pessoas FOREIGN KEY (IdPessoa) REFERENCES Pessoas(IdPessoa) 
)
GO

CREATE TABLE Emprestimos(
	IdEmprestimo INT NOT NULL IDENTITY(1,1),
	DataEmprestimo DATE NOT NULL,
	DataPrevistaDevolucao DATE NOT NULL,
	DataDevolucao DATE NOT NULL,
	IdUsuario INT NOT NULL,
	DataCadastro INT NOT NULL
		
	CONSTRAINT PK_Emprestimos PRIMARY KEY (IdEmprestimo),
	CONSTRAINT FK_Emprestimo_Usuarios FOREIGN KEY (IdUsuario) REFERENCES Usuarios(IdUsuario) 
)
GO

CREATE TABLE Reservas(
	IdReserva INT NOT NULL IDENTITY(1,1),
	DataReserva DATE NOT NULL,
	DataValidade DATE NOT NULL,
	IdUsuario INT NOT NULL,
	DataCadastro DATE NOT NULL,
	Ativo BIT NOT NULL
		
	CONSTRAINT PK_Reservas PRIMARY KEY (IdReserva),
	CONSTRAINT FK_Reservas_Usuarios FOREIGN KEY (IdUsuario) REFERENCES Usuarios(IdUsuario) 
)
GO

CREATE TABLE Assuntos(
	IdAssunto INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(50) NOT NULL
		
	CONSTRAINT PK_Assunto PRIMARY KEY (IdAssunto) 
)
GO

CREATE TABLE Editora(
	IdEditora INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(50) NOT NULL
		
	CONSTRAINT PK_Editora PRIMARY KEY (IdEditora) 
)
GO

CREATE TABLE Autores(
	IdAutor INT NOT NULL IDENTITY(1,1),
	Nome NVARCHAR(50) NOT NULL,
	Email NVARCHAR(100) NOT NULL,
	DataCadastro DATE NOT NULL,	
	Ativo BIT NOT NULL

	CONSTRAINT PK_Autores PRIMARY KEY (IdAutor),
)
GO

CREATE TABLE Artigos(
	IdArtigo INT NOT NULL IDENTITY(1,1),
	Titulo NVARCHAR(50) NOT NULL,
	DataCadastro DATE NOT NULL,
	Ativo BIT NOT NULL
		
	CONSTRAINT PK_Artigos PRIMARY KEY (IdArtigo) 
)
GO

CREATE TABLE ArtigosAutores(
	IdArtigoAutor INT NOT NULL IDENTITY(1,1),
	IdArtigo INT NOT NULL,
	IdAutor INT NOT NULL
		
	CONSTRAINT PK_ArtigosAutores PRIMARY KEY (IdArtigoAutor),
	CONSTRAINT FK_ArtigosAutores_Artigos FOREIGN KEY (IdArtigo) REFERENCES Artigos(IdArtigo),
	CONSTRAINT FK_ArtigoAutores_Autores FOREIGN KEY (IdAutor) REFERENCES Autores(IdAutor) 
)
GO

CREATE TABLE Materiais(
	IdMaterial INT NOT NULL IDENTITY(1,1),
	Titulo NVARCHAR(50) NOT NULL,
	IdAssunto INT NOT NULL,
	IdEditora INT NOT NULL,
	DataCadastro DATE NOT NULL,
	Ativo BIT NOT NULL
		
	CONSTRAINT PK_Materiais PRIMARY KEY (IdMaterial),
	CONSTRAINT FK_Materias_Assuntos FOREIGN KEY (IdAssunto) REFERENCES Assuntos(IdAssunto),
	CONSTRAINT FK_Materiais_Editora FOREIGN KEY (IdEditora) REFERENCES Editora(IdEditora) 
)
GO

CREATE TABLE MateriaisReserva(
	IdMaterialReserva INT NOT NULL IDENTITY(1,1),
	IdMaterial INT NOT NULL,
	IdReserva INT NOT NULL
		
	CONSTRAINT PK_MateriaisReserva PRIMARY KEY (IdMaterialReserva),
	CONSTRAINT FK_MateriaisReserva_Material FOREIGN KEY (IdMaterial) REFERENCES Materiais(IdMaterial),
	CONSTRAINT FK_MateriaisReserva_Reserva FOREIGN KEY (IdReserva) REFERENCES Reservas(IdReserva) 
)
GO

CREATE TABLE MateriaisEmprestimos(
	IdMaterialEmprestimo INT NOT NULL IDENTITY(1,1),
	IdMaterial INT NOT NULL,
	IdEmprestimo INT NOT NULL
		
	CONSTRAINT PK_MateriaisEmprestimos PRIMARY KEY (IdMaterialEmprestimo),
	CONSTRAINT FK_MateriaisEmprestimos_Materiais FOREIGN KEY (IdMaterial) REFERENCES Materiais(IdMaterial),
	CONSTRAINT FK_MateriaisEmprestimos_EmprestimoS FOREIGN KEY (IdEmprestimo) REFERENCES Emprestimos(IdEmprestimo)
)
GO

CREATE TABLE Revistas(
	IdRevista INT NOT NULL IDENTITY(1,1),
	Colecao NVARCHAR(50) NOT NULL,
	DataCadastro DATE NOT NULL,
	Ativo BIT NOT NULL,
	IdMaterial INT NOT NULL

	CONSTRAINT PK_Revistas PRIMARY KEY (IdRevista),
	CONSTRAINT FK_Revista_Materiais FOREIGN KEY (IdMaterial) REFERENCES Materiais(IdMaterial) 
)
GO

CREATE TABLE RevistaArtigos(
	IdRevistaArtigo INT NOT NULL IDENTITY(1,1),
	IdRevista INT NOT NULL,
	IdArtigo INT NOT NULL
		
	CONSTRAINT PK_RevistaArtigos PRIMARY KEY (IdRevistaArtigo),
	CONSTRAINT FK_RevistaArtigos_Revistas FOREIGN KEY (IdRevista) REFERENCES Revistas(IdRevista),
	CONSTRAINT FK_RevistaArtigos_Artigo  FOREIGN KEY (IdArtigo) REFERENCES Artigos(IdArtigo) 
)
GO

CREATE TABLE Livros(
	IdLivro INT NOT NULL IDENTITY(1,1),
	Edicao NVARCHAR(50) NOT NULL,
	Isbn NVARCHAR(17) NOT NULL,
	DataCadastro DATE NOT NULL,
	IdMaterial INT NOT NULL
		
	CONSTRAINT PK_Livros PRIMARY KEY (IdLivro),
	CONSTRAINT FK_Livros_Materiais FOREIGN KEY (IdMaterial) REFERENCES Materiais(IdMaterial)
)
GO

CREATE TABLE LivrosAutores(
	IdLivroAutor INT NOT NULL IDENTITY(1,1),
	IdLivro INT NOT NULL,
	IdAutor INT NOT NULL
		
	CONSTRAINT PK_LivrosAutores PRIMARY KEY (IdLivroAutor),
	CONSTRAINT FK_LivrosAutores_Livro FOREIGN KEY (IdLivro) REFERENCES Livros(IdLivro),
	CONSTRAINT FK_LivrosAutores_Autores FOREIGN KEY (IdAutor) REFERENCES Autores(IdAutor)
)
GO

