﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Endereco : Entity<Endereco>
    {
        #region Propriedades

        public string Logradouro { get; private set; }
        public string Bairro { get; private set; }
        public string Cep { get; private set; }
        public int Numero { get; private set; }
        public string Complemento { get; private set; }
        public Cidade Cidade { get; private set; }

        #endregion

        #region Construtor
        public Endereco(string logradouro, string bairro, string cep, int numero, string complemento, int idCidade)
        {
            Logradouro = logradouro;
            Bairro = bairro;
            Cep = cep;
            Numero = numero;
            Complemento = complemento;
            Cidade = Cidade.CidadeFactory.Criar(idCidade);
        }

        private Endereco() { }
        #endregion

        #region Factory
        public static class EnderecoFactory
        {
            public static Endereco Criar(int id)
            {
                return new Endereco { Id = id };
            }

            public static Endereco Criar(string cep)
            {
                return new Endereco { Cep = cep };
            }
        }
        #endregion

        #region Métodos

        /// <summary>
        /// Adhoc setter
        /// </summary>
        void AtribuirCidade(Cidade cidade)
        {
            if (!cidade.Valido()) return;

            Cidade = cidade;
        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Logradouro)
                .NotEmpty().WithMessage("O logradouro é obrigatório")
                .MaximumLength(255).WithMessage("O logradouro não pode ser maior que 255 caracteres");

            RuleFor(x => x.Bairro)
                .NotEmpty().WithMessage("O bairro é obrigatório")
                .MaximumLength(100).WithMessage("O bairro não pode ser maior que 100 caracteres");

            RuleFor(x => x.Cep)
                .NotEmpty().WithMessage("O CEP é obrigatório")
                .Length(9).WithMessage("O CEP deve possuir 9 caracteres");

            RuleFor(x => x.Numero)
                .NotEmpty().WithMessage("O número do endereço é obrigatório")
                .GreaterThan(0).WithMessage("O número do endereço deve ser maior que 0");

            ValidationResult = Validate(this);

            ValidarCidade();
        }

        private void ValidarCidade()
        {
            if (Cidade.Valido())
                return;

            foreach(var error in Cidade.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        #endregion
    }
}
