﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Usuario : Entity<Usuario>
    {
        private int p1;
        private string p2;
        private string p3;

        public string Email { get; private set; }
        public string Senha { get; private set; }
        public Pessoa Pessoa { get; private set; }

        public Usuario(int id, string email, string senha, int idPessoa)
        {
            Id = id;
            Email = email;
            Senha = senha;
            Pessoa = Pessoa.PessoaFactory.Criar(idPessoa);
        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("O e-mail do usuário é obrigatório")
                .EmailAddress().WithMessage("O e-mail do usuário deve ser válido");

            RuleFor(x => x.Senha)
                .NotEmpty().WithMessage("A senha do usuário é obrigatória");

            ValidationResult = Validate(this);

            ValidarPessoa();
        }

        private void ValidarPessoa()
        {
            if (Pessoa.Valido()) return;

            foreach (var error in Pessoa.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        #region Adhoc Setter
        public void AtribuirPessoa(Pessoa pessoa)
        {
            if (!pessoa.Valido()) return;

            Pessoa = pessoa;
        }
        #endregion

        public void AtribuirSenha(string p)
        {
            throw new NotImplementedException();
        }
    }
}
