﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Editora : Entity<Editora>
    {
        public string Nome { get; private set; }

        public Editora()
        {

        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O nome da editora é obrigatório")
                .Length(2, 150).WithMessage("O nome da editora deve ter entre 2 e 150 caracteres");

            ValidationResult = Validate(this);
        }

        public static class EditoraFactory
        {
            public static Editora Criar (int id)
            {
                return new Editora { Id = id };
            }
        }
    }
}
