﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Assunto : Entity<Assunto>
    {
        public string Nome { get; private set; }

        public Assunto()
        {

        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O nome do assunto é obrigatório.")
                .Length(2, 150).WithMessage("O nome do assunto deve ter entre 2 e 150 caracteres");


            ValidationResult = Validate(this);
        }

        public static class AssuntoFactory
        {
            public static Assunto Criar(int id)
            {
                return new Assunto { Id = id };
            }
        }

    }
}
