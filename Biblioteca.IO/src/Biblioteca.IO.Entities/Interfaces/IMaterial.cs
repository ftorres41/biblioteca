﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Entities.Interfaces
{
    interface IMaterial
    {
        string Titulo { get; }
        Assunto Assunto { get; }
        Editora Editora { get; }

        void AtribuirAssunto(Assunto assunto);

        void AtribuirEditora(Editora editora);
    }
}
