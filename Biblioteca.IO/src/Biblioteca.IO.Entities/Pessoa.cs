﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Pessoa : Entity<Pessoa>
    {
        #region Propriedades

        public string Nome { get; private set; }
        public string Rg { get; private set; }
        public string Cpf { get; private set; }
        public List<Telefone> Telefones { get; private set; }
        public DateTime DataNascimento { get; private set; }
        public Endereco Endereco { get; private set; }

        #endregion

        #region Construtor
        public Pessoa(string nome, string rg, string cpf, List<int> telefones, DateTime dataNascimento, int idEndereco)
        {
            Nome = nome;
            Rg = rg;
            Cpf = cpf;
            DataNascimento = dataNascimento;
            Telefones = Telefone.TelefoneFactory.Criar(telefones);
            Endereco = Endereco.EnderecoFactory.Criar(idEndereco);
        }

        private Pessoa() { }
        #endregion

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O nome da pessoa é obrigatório")
                .Length(2, 250).WithMessage("O nome da pessoa deve ter entre 2 e 150 caracteres");

            RuleFor(x => x.Rg)
                .NotEmpty().WithMessage("O RG da pessoa é obrigatório")
                .Length(11).WithMessage("O RG da pessoa deve ter 11 caracteres");

            RuleFor(x => x.Cpf)
                .NotEmpty().WithMessage("O CPF da pessoa é obrigatório")
                .Length(15).WithMessage("O CPF da pessoa deve ter 15 caracteres");

            RuleFor(x => x.DataNascimento)
                .NotEmpty().WithMessage("A data de nascimento da pessoa é obrigatória");

            ValidationResult = Validate(this);

            ValidarTelefones();

            ValidarEndereco();
        }

        private void ValidarEndereco()
        {
            if (Endereco.Valido())
                return;

            foreach (var error in Endereco.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        private void ValidarTelefones()
        {
            foreach (var telefone in Telefones)
            {
                if (telefone.Valido())
                    return;

                foreach (var error in telefone.ValidationResult.Errors)
                {
                    ValidationResult.Errors.Add(error);
                }
            }
        }

        #region Factory
        public static class PessoaFactory
        {
            public static Pessoa Criar(int id)
            {
                return new Pessoa { Id = id };
            }

            public static Pessoa Criar(string nome)
            {
                return new Pessoa { Nome = nome };
            }
        }
        #endregion
    }
}
