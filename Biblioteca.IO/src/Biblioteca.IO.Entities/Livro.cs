﻿using Biblioteca.IO.Entities.Core;
using Biblioteca.IO.Entities.Interfaces;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Livro : Entity<Livro>, IMaterial
    {
        public string Edicao { get; private set; }
        public string Isbn { get; private set; }
        public List<Autor> Autores { get; private set; }
        public string Titulo { get; private set; }
        public Assunto Assunto { get; private set; }
        public Editora Editora { get; private set; }

        public Livro(string titulo, int idAssunto,int idEditora, string edicao, string isbn, List<int> autores)
        {
            Titulo = titulo;
            Edicao = edicao;
            Isbn = isbn;
            Autores = Autor.AutorFactory.Criar(autores);
            Editora = Editora.EditoraFactory.Criar(idEditora);
            Assunto = Assunto.AssuntoFactory.Criar(idAssunto);
        }

        public void AtribuirAssunto(Assunto assunto)
        {
            if (!assunto.Valido()) return;

            Assunto = assunto;
        }

        public void AtribuirEditora(Editora editora)
        {
            if (!editora.Valido()) return;

            Editora = editora;
        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Edicao)
                .NotEmpty().WithMessage("A edição da revista é obrigatória")
                .Length(25).WithMessage("A edição não deve exceder 25 caracteres");

            RuleFor(x => x.Titulo)
                .NotEmpty().WithMessage("O título da revista é obrigatório")
                .Length(100).WithMessage("O título da revista não deve exceder 100 caracteres");

            RuleFor(x => x.Isbn)
                .NotEmpty().WithMessage("O ISBN da revista é obrigatório")
                .Length(25).WithMessage("O ISBN da revista não deve exceder 25 caracteres");

            ValidationResult = Validate(this);

            ValidarAutores();

            ValidarAssunto();

            ValidarEditora();
        }

        private void ValidarAutores()
        {
            foreach (var autor in Autores)
            {
                if (autor.Valido()) return;

                foreach (var error in autor.ValidationResult.Errors)
                {
                    ValidationResult.Errors.Add(error);
                }
            }
        }

        private void ValidarAssunto()
        {
            if (Assunto.Valido()) return;

            foreach (var error in Assunto.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        private void ValidarEditora()
        {
            if (Editora.Valido()) return;

            foreach (var error in Editora.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }
    }
}
