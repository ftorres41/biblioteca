﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Emprestimo : Entity<Emprestimo>
    {
        public DateTime DataEmprestimo { get; private set; }
        public DateTime DataPrevistaRetorno { get; private set; }
        public DateTime DataRetorno { get; private set; }
        public List<Material> Materiais { get; private set; }
        public Usuario Usuario { get; private set; }

        public Emprestimo(DateTime emprestimo, DateTime previsao, DateTime retorno, List<int> materiais, Usuario usuario)
        {
            DataEmprestimo = emprestimo;
            DataPrevistaRetorno = previsao;
            DataRetorno = retorno;
            Materiais = Material.MaterialFactory.Criar(materiais);
            Usuario = usuario;
        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.DataEmprestimo)
                .NotEmpty().WithMessage("A data de empréstimo é obrigatória")
                .Equal(DateTime.Today).WithMessage("A data de empréstimo deve ser a de hoje");

            RuleFor(x => x.DataPrevistaRetorno)
                .NotEmpty().WithMessage("A data prevista para o retorno é obrigatória")
                .Equal(DateTime.Today.AddDays(7)).WithMessage("A data prevista para o retorno não deve ser superior a 7 dias do empréstimo");

            ValidarMateriais();
            ValidarUsuario();
        }

        private void ValidarMateriais()
        {
            foreach (var material in Materiais)
            {
                if (material.Valido()) return;

                foreach (var error in material.ValidationResult.Errors)
                {
                    ValidationResult.Errors.Add(error);
                }
            }
        }

        private void ValidarUsuario()
        {
            if (Usuario.Valido()) return;

            foreach (var error in Usuario.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }
    }
}
