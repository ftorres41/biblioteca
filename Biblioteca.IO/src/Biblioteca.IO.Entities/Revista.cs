﻿using Biblioteca.IO.Entities.Core;
using Biblioteca.IO.Entities.Interfaces;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Revista : Entity<Revista>, IMaterial
    {
        public string Colecao { get; private set; }
        public string Titulo { get; private set; }
        public List<Artigo> Artigos { get; private set; }
        public Assunto Assunto { get; private set; }
        public Editora Editora { get; private set; }

        #region Construtor
        public Revista(string titulo, int idAssunto, int idEditora, string colecao, List<int> artigos)
        {
            Colecao = colecao;
            Titulo = titulo;
            Artigos = Artigo.ArtigoFactory.Criar(artigos);
            Editora = Editora.EditoraFactory.Criar(idEditora);
            Assunto = Assunto.AssuntoFactory.Criar(idAssunto);
        }
        #endregion

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Colecao)
                .NotEmpty().WithMessage("A coleção da revista é obrigatória")
                .Length(100).WithMessage("A coleção não deve exceder 100 caracteres");

            RuleFor(x => x.Titulo)
                .NotEmpty().WithMessage("O título da revista é obrigatório")
                .Length(100).WithMessage("O título da revista não deve exceder 100 caracteres");

            ValidationResult = Validate(this);

            ValidarArtigos();

            ValidarAssunto();

            ValidarEditora();
        }

        private void ValidarArtigos()
        {
            foreach (var artigo in Artigos)
            {
                if (artigo.Valido()) return;

                foreach (var error in artigo.ValidationResult.Errors)
                {
                    ValidationResult.Errors.Add(error);
                }
            }
        }

        private void ValidarAssunto()
        {
            if (Assunto.Valido()) return;

            foreach (var error in Assunto.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        private void ValidarEditora()
        {
            if (Editora.Valido()) return;

            foreach (var error in Editora.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        public void AtribuirAssunto(Assunto assunto)
        {
            if (!assunto.Valido()) return;

            Assunto = assunto;
        }


        public void AtribuirEditora(Editora editora)
        {
            if (!editora.Valido()) return;

            Editora = editora;
        }
    }
}
