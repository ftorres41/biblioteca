﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Reserva : Entity<Reserva>
    {
        public DateTime DataReserva { get; private set; }
        public DateTime DataValidade { get; private set; }
        public List<Material> Materiais { get; private set; }
        public Usuario Usuario { get; private set; }

        #region Construtor
        public Reserva(DateTime reserva, DateTime validade, Usuario usuario)
        {
            DataReserva = reserva;
            DataValidade = validade;
            Materiais = new List<Material>();
            Usuario = usuario;
        }
        #endregion

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.DataReserva)
                .NotEmpty().WithMessage("A data de reserva é obrigatória");

            RuleFor(x => x.DataValidade)
                .NotEmpty().WithMessage("A data de validade da reserva é obrigatória");

            ValidationResult = Validate(this);

            ValidarMateriais();

            ValidarUsuario();
        }

        private void ValidarMateriais()
        {
            foreach (var material in Materiais)
            {
                if (!material.Valido()) return;

                foreach (var error in material.ValidationResult.Errors)
                {
                    ValidationResult.Errors.Add(error);
                }
            }
        }

        private void ValidarUsuario()
        {
            if (!Usuario.Valido()) return;

            foreach (var error in Usuario.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }
    }
}
