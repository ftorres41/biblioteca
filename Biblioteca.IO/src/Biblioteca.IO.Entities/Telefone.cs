﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Telefone : Entity<Telefone>
    {
        public int IdPessoa { get; private set; }
        public string Numero { get; private set; }
        public TipoTelefone TipoTelefone { get; private set; }

        #region Construtores
        public Telefone(string numero, int idTipoTelefone)
        {
            Numero = numero;
            TipoTelefone = TipoTelefone.TipoTelefoneFactory.Criar(idTipoTelefone);
        }

        private Telefone()
        {

        }
        #endregion


        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.IdPessoa)
                .NotEmpty().WithMessage("O ID da pessoa para o telefone é obrigatório");

            RuleFor(x => x.Numero)
                .NotEmpty().WithMessage("O número de telefone é obrigatório")
                .Length(14, 15).WithMessage("O número de telefone deve ter entre 14 e 15 dígitos, incluindo DDD");

            ValidationResult = Validate(this);

            ValidarTipoTelefone();
        }

        private void ValidarTipoTelefone()
        {
            if (TipoTelefone.Valido()) return;

            foreach (var error in TipoTelefone.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        public static class TelefoneFactory
        {
            public static List<Telefone> Criar(List<int> ids)
            {
                var lista = new List<Telefone>();

                for(int i = 0; i < ids.Count(); i++)
                {
                    lista.Add(new Telefone { Id = ids[i] });
                }

                return lista;
            }
        }
    }
}
