﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Cidade : Entity<Cidade>
    {
        #region Propriedades

        public string Nome { get; private set; }
        public Estado Estado { get; private set; }

        #endregion

        #region Construtor

        public Cidade(int id, string nome, int idEstado)
        {
            Id = id;
            Nome = nome;
            Estado = Estado.EstadoFactory.Criar(idEstado);
        }

        private Cidade() { }

        #endregion

        #region Factory

        public static class CidadeFactory
        {
            public static Cidade Criar(int id)
            {
                return new Cidade { Id = id };
            }

            public static Cidade Criar(string nome)
            {
                return new Cidade { Nome = nome };
            }
        }

        #endregion

        #region Métodos

        /// <summary>
        /// Adhoc setter for Cidade
        /// </summary>
        void AtribuirEstado(Estado estado)
        {
            if (!estado.Valido()) return;

            Estado = estado;
        }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O nome da cidade é obrigatório")
                .MaximumLength(50).WithMessage("O nome da cidade não pode exceder 50 caracteres");

            ValidationResult = Validate(this);

            ValidarEstado();
        }

        private void ValidarEstado()
        {
            if (Estado.Valido())
                return;

            foreach (var error in Estado.ValidationResult.Errors)
            {
                ValidationResult.Errors.Add(error);
            }
        }

        #endregion
    }
}
