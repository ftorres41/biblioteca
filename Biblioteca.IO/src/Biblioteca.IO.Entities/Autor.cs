﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Autor : Entity<Autor>
    {
        public string Nome { get; private set; }
        public string Email { get; private set; }

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O nome do autor é obrigatório")
                .Length(2, 100).WithMessage("O nome do autor deve ter entre 2 e 100 caracteres");

            RuleFor(x => x.Email)
                .EmailAddress().WithMessage("O e-mail deve ser válido")
                .NotEmpty().WithMessage("O e-mail é obrigatório");

            ValidationResult = Validate(this);
        }

        public static class AutorFactory
        {
            public static List<Autor> Criar(List<int> ids)
            {
                var lista = new List<Autor>();

                for (int i = 0; i < ids.Count(); i++)
                {
                    lista.Add(new Autor { Id = ids[i] });
                }

                return lista;
            }
        }
    }
}
