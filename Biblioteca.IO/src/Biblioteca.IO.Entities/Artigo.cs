﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Artigo : Entity<Artigo>
    {
        #region Propriedades

        public string Titulo { get; private set; }
        public List<Autor> Autores { get; private set; }

        #endregion

        #region Construtor

        public Artigo(int id, string titulo, List<int> autores)
        {
            Id = id;
            Titulo = titulo;
            Autores = Autor.AutorFactory.Criar(autores);
        }

        private Artigo() { }

        #endregion

        #region Métodos

        public override bool Valido()
        {
            Validar();
            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Titulo)
                .NotEmpty().WithMessage("O título do artigo é obrigatório")
                .Length(2, 150).WithMessage("O título do artigo deve ter entre 2 e 150 caracteres");

            ValidationResult = Validate(this);

            ValidarAutores();
        }

        private void ValidarAutores()
        {
            foreach (var autor in Autores)
            {
                if (!autor.Valido()) return;

                foreach (var error in autor.ValidationResult.Errors)
                {
                    ValidationResult.Errors.Add(error);
                }
            }
        }

        #endregion

        #region Factory
        public static class ArtigoFactory
        {
            public static List<Artigo> Criar (List<int> ids)
            {
                var lista = new List<Artigo>();

                for (int i = 0; i < ids.Count(); i++)
                {
                    lista.Add(new IO.Artigo { Id = ids[i] });
                }

                return lista;
            }
        }

        #endregion
    }
}
