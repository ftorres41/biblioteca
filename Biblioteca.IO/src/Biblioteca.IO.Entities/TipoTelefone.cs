﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class TipoTelefone : Entity<TipoTelefone>
    {
        public string Nome { get; private set; }

        #region Construtor
        private TipoTelefone()
        {

        }
        #endregion

        public override bool Valido()
        {
            throw new NotImplementedException();
        }

        public static class TipoTelefoneFactory
        {
            public static TipoTelefone Criar(int id)
            {
                return new TipoTelefone { Id = id };
            }
        }
    }
}
