﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Estado : Entity<Estado>
    {
        #region Propriedades

        public string Nome { get; private set; }
        public string Sigla { get; private set; }

        #endregion

        #region Construtor

        public Estado(int id, string nome, string sigla)
        {
            Id = id;
            Nome = nome;
            Sigla = sigla;
        }

        private Estado() { }

        #endregion

        #region Factory

        public static class EstadoFactory
        {
            public static Estado Criar(int id)
            {
                return new Estado { Id = id };
            }

            public static Estado Criar(string nome)
            {
                return new Estado { Nome = nome };
            }
        }

        #endregion

        #region Métodos

        public override bool Valido()
        {
            Validar();

            return ValidationResult.IsValid;
        }

        private void Validar()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O nome do estado é obrigatório")
                //.MaximumLength(50).WithMessage("O nome do estado deve ter no máximo 50 caracteres")
                .Length(4, 50).WithMessage("O nome do estado deve ter entre 2 e 50 caracteres");

            RuleFor(x => x.Sigla)
                .NotEmpty().WithMessage("A sigla do estado é obrigatória")
                .Length(2).WithMessage("A sigla deve ter exatamente 2 caracteres.");

            ValidationResult = Validate(this);
        }

        #endregion
    }
}
