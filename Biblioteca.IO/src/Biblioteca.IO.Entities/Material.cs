﻿using Biblioteca.IO.Entities.Core;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public abstract class Material : Entity<Material>
    {
        public string Titulo { get; protected set; }
        public Assunto Assunto { get; protected set; }
        public Editora Editora { get; protected set; }

        #region Construtor
        public Material(string titulo, int idAssunto, int idEditora)
        {
            Titulo = titulo;
            Assunto = Assunto.AssuntoFactory.Criar(idAssunto);
            Editora = Editora.EditoraFactory.Criar(idEditora);
        }

        private Material() { }
        #endregion

        #region Adhoc Setter
        public void AtribuirAssunto(Assunto assunto)
        {
            if (!assunto.Valido()) return;

            Assunto = assunto;
        }

        public void AtribuirEditora(Editora editora)
        {
            if (!editora.Valido()) return;

            Editora = editora;
        }
        #endregion

        #region Factory
        public static class MaterialFactory
        {
            public static List<Material> Criar(List<int> ids)
            {
                var lista = new List<Material>();

                for (int i = 0; i < ids.Count(); i++)
                {
                    //lista.Add(new Material { Id = ids[i] });
                }

                return lista;
            }
        }
        #endregion
    }
}
