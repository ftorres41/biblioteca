﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Repositories
{
    public class ReservaRepository : IReservaRepository
    {
        public void Atualizar(Reserva obj)
        {
            throw new NotImplementedException();
        }

        public void CancelarReserva(List<Material> materiais, Usuario usuario)
        {
            throw new NotImplementedException();
        }

        public void DeletarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public void Inserir(Reserva obj)
        {
            throw new NotImplementedException();
        }

        public Reserva ObterPorId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Reserva> ObterPorMaterial(string titulo)
        {
            throw new NotImplementedException();
        }

        public List<Reserva> ObterPorPeriodo(DateTime dataInicio, DateTime dataFim)
        {
            throw new NotImplementedException();
        }

        public List<Reserva> ObterPorUsuario(int id)
        {
            throw new NotImplementedException();
        }

        public void RealizarReserva(List<Material> materiais, Usuario usuario)
        {
            throw new NotImplementedException();
        }
    }
}
