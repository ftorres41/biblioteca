﻿using Biblioteca.IO.CrossCutting.Exceptions;
using Biblioteca.IO.Entities;
using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private const string ConnectionString = "Server=myServerAddress;Database=myDataBase;User Id=myUsername; Password=myPassword;";

        public void AlterarSenha(string email)
        {
            throw new NotImplementedException();
        }

        public void AtribuirPessoa(string email)
        {
            throw new NotImplementedException();
        }

        public void Atualizar(Usuario usuario)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = "spAlterarUsuario",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Id", usuario.Id);
                command.Parameters.AddWithValue("Email", usuario.Email);
                command.Parameters.AddWithValue("Senha", usuario.Senha);

                command.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        public void CadastrarUsuario(Usuario usuario)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = "spInserirUsuario",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Email", usuario.Email);
                command.Parameters.AddWithValue("Senha", usuario.Senha);

                command.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        public void DeletarPorId(int id)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = "spDeletarUsuario",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Id", id);

                command.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        public void DesativarUsuario(string email)
        {
            throw new NotImplementedException();
        }

        public void Inserir(Usuario usuario)
        {
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = "spInserirUsuario",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Email", usuario.Email);
                command.Parameters.AddWithValue("Senha", usuario.Senha);

                command.ExecuteNonQuery();
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        public Usuario ObterPorEmail(string email)
        {
            throw new NotImplementedException();
        }

        public Usuario ObterPorId(int id)
        {
            SqlConnection conn = null;
            Usuario usuario = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                SqlCommand command = new SqlCommand
                {
                    CommandText = "spObterPorId",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Id", id);

                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        var idUsuario = reader.GetInt32(reader.GetOrdinal("IdUsuarios"));
                        var email = reader["Email"].ToString();
                        var senha = reader["Senha"].ToString();
                        var idPessoa = reader.GetInt32(reader.GetOrdinal("IdPessoa"));

                        usuario = new Usuario(idUsuario, email, senha, idPessoa);
                    }
                }

                return usuario != null ? usuario : null;
            }
            catch (UsuarioNaoEncontradoException)
            {
                throw new UsuarioNaoEncontradoException();
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
        }

        public bool VerificarUsuarioDesativado(string email)
        {
            throw new NotImplementedException();
        }

        public bool VerificarUsuarioExistente(string email)
        {
            throw new NotImplementedException();
        }
    }
}
