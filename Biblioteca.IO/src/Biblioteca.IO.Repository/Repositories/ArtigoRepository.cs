﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository
{
    public class ArtigoRepository : IArtigoRepository
    {
        public void Atualizar(Artigo obj)
        {
            throw new NotImplementedException();
        }

        public void DeletarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public void Inserir(Artigo obj)
        {
            throw new NotImplementedException();
        }

        public List<Artigo> ObterPorAutor(string autor)
        {
            throw new NotImplementedException();
        }

        public Artigo ObterPorId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Artigo> ObterPorRevista(string colecao)
        {
            throw new NotImplementedException();
        }
    }
}
