﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository
{
    public class CidadeRepository : ICidadeRepository
    {
        private const string ConnectionString = "Server=myServerAddress;Database=myDataBase;User Id=myUsername; Password=myPassword;";

        public Cidade ObterPorId(int id)
        {
            Cidade cidade = null;

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterCidadePorId",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Id", id);

                using (var dataReader = command.ExecuteReader())
                {
                    cidade = PreencherCidade(dataReader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return cidade;
        }

        public Cidade ObterPorNome(string nome)
        {
            Cidade cidade = null;

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterCidadePorNome",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Nome", nome);

                using (var dataReader = command.ExecuteReader())
                {
                    cidade = PreencherCidade(dataReader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return cidade;
        }

        public IList<Cidade> ObterPorEstado(int idEstado)
        {
            var cidades = new List<Cidade>();

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterCidadePorEstado",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("IdEstado", idEstado);

                using (var dataReader = command.ExecuteReader())
                {
                    cidades.Add(PreencherCidade(dataReader));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return cidades;
        }

        public IList<Cidade> ObterTodos()
        {
            var cidades = new List<Cidade>();

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterCidades",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                using (var dataReader = command.ExecuteReader())
                {
                    cidades.Add(PreencherCidade(dataReader));
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return cidades;
        }

        private Cidade PreencherCidade(SqlDataReader reader)
        {
            if (reader.Read())
            {
                var idCidade = reader.GetInt32(reader.GetOrdinal("IdCidade"));
                var nomeEstado = reader["Nome"].ToString();
                var idEstado = reader.GetInt32(reader.GetOrdinal("IdEstado"));

                return new Cidade(idCidade, nomeEstado, idEstado);
            }
            else
            {
                return null;
            }
        }
    }
}
