﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Repositories
{
    public class EmprestimoRepository : IEmprestimoRepository
    {
        public void Atualizar(Emprestimo obj)
        {
            throw new NotImplementedException();
        }

        public void DeletarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public void Inserir(Emprestimo obj)
        {
            throw new NotImplementedException();
        }

        public List<Emprestimo> ObterEmprestimosAtrasados(DateTime dataPrevista)
        {
            throw new NotImplementedException();
        }

        public List<Emprestimo> ObterPorDataPrevistaRetorno(DateTime dataPrevista)
        {
            throw new NotImplementedException();
        }

        public Emprestimo ObterPorId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Emprestimo> ObterPorUsuario(int id)
        {
            throw new NotImplementedException();
        }

        public void RealizarDevolucao(Usuario usuario, List<Material> materiais)
        {
            throw new NotImplementedException();
        }

        public void RealizarEmprestimo(Usuario usuario, List<Material> materiais)
        {
            throw new NotImplementedException();
        }
    }
}
