﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Repositories
{
    public class MaterialRepository : IMaterialRepository
    {
        public void AtribuirAssunto()
        {
            throw new NotImplementedException();
        }

        public void AtribuirEditora()
        {
            throw new NotImplementedException();
        }

        public void Atualizar(Material obj)
        {
            throw new NotImplementedException();
        }

        public void DeletarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public void Inserir(Material obj)
        {
            throw new NotImplementedException();
        }

        public List<Material> ObterPorEditora(string nomeEditora)
        {
            throw new NotImplementedException();
        }

        public Material ObterPorId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Material> ObterPorTitulo(string titulo)
        {
            throw new NotImplementedException();
        }
    }
}
