﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Repositories
{
    public class LivroRepository : ILivroRepository
    {
        public void AlterarLivro(string nome)
        {
            throw new NotImplementedException();
        }

        public void Atualizar(Livro obj)
        {
            throw new NotImplementedException();
        }

        public void CadastrarLivro()
        {
            throw new NotImplementedException();
        }

        public void DeletarPorId(int id)
        {
            throw new NotImplementedException();
        }

        public void Inserir(Livro obj)
        {
            throw new NotImplementedException();
        }

        public List<Livro> ObterPorAutor(string autor)
        {
            throw new NotImplementedException();
        }

        public Livro ObterPorId(int id)
        {
            throw new NotImplementedException();
        }

        public Livro ObterPorIsbn(string isbn)
        {
            throw new NotImplementedException();
        }
    }
}
