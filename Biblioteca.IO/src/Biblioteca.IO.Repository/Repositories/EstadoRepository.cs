﻿using Biblioteca.IO.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository
{
    public class EstadoRepository : IEstadoRepository
    {
        private const string ConnectionString = "Server=myServerAddress;Database=myDataBase;User Id=myUsername; Password=myPassword;";

        public Estado ObterPorId(int id)
        {
            Estado estado = null;

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterEstadoPorId",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Id", id);

                using(var dataReader = command.ExecuteReader())
                {
                    estado = PreencherEstado(dataReader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return estado;
        }

        public Estado ObterPorNome(string nome)
        {
            Estado estado = null;

            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterEstadoPorNome",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                command.Parameters.AddWithValue("Nome", nome);

                using (var dataReader = command.ExecuteReader())
                {
                    estado = PreencherEstado(dataReader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return estado;
        }

        public IList<Estado> ObterTodos()
        {
            var estados = new List<Estado>();
            SqlConnection conn = null;

            try
            {
                conn = new SqlConnection(ConnectionString);
                conn.Open();

                var command = new SqlCommand
                {
                    CommandText = "spObterEstados",
                    CommandType = CommandType.StoredProcedure,
                    Connection = conn
                };

                using (var dataReader = command.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        estados.Add(PreencherEstado(dataReader));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }

            return estados;
        }

        private Estado PreencherEstado(SqlDataReader reader)
        {
            if (reader.Read())
            {
                var idEstado = reader.GetInt32(reader.GetOrdinal("IdEstado"));
                var nomeEstado = reader["Nome"].ToString();
                var siglaEstado = reader["Sigla"].ToString();

                return new Estado(idEstado, nomeEstado, siglaEstado);
            }
            else
            {
                return null;
            }
        }
    }
}
