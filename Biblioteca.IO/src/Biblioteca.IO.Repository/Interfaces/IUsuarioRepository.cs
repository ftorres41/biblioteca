﻿using Biblioteca.IO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IUsuarioRepository : IRepository<Usuario>
    {
        Usuario ObterPorId(int id);

        void AtribuirPessoa(string email);

        Usuario ObterPorEmail(string email);

        bool VerificarUsuarioExistente(string email);

        bool VerificarUsuarioDesativado(string email);

        void AlterarSenha(string email);

        void DesativarUsuario(string email);

        void CadastrarUsuario(Usuario usuario);
    }
}
