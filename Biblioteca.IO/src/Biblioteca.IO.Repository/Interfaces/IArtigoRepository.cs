﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IArtigoRepository : IRepository<Artigo>
    {
        List<Artigo> ObterPorRevista(string colecao);

        List<Artigo> ObterPorAutor(string autor);
    }
}
