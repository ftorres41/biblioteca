﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface ILivroRepository : IRepository<Livro>
    {
        Livro ObterPorIsbn(string isbn);

        List<Livro> ObterPorAutor(string autor);

        void CadastrarLivro();

        void AlterarLivro(string nome);
    }
}
