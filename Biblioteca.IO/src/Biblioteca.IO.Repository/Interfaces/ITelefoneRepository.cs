﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface ITelefoneRepository : IRepository<Telefone>
    {
        List<Telefone> ObterPorPessoa(int id);

        void AtribuirTipoTelefone();
    }
}
