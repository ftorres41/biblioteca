﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IReservaRepository : IRepository<Reserva>
    {
        void RealizarReserva(List<Material> materiais, Usuario usuario);

        void CancelarReserva(List<Material> materiais, Usuario usuario);

        List<Reserva> ObterPorUsuario(int id);

        List<Reserva> ObterPorMaterial(string titulo);

        List<Reserva> ObterPorPeriodo(DateTime dataInicio, DateTime dataFim);
    }
}
