﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IPessoaRepository : IRepository<Pessoa>
    {
        void CadastrarPessoa();

        void AtribuirEndereco();

        void AtribuirTelefones();

        void AtualizarEndereco();

        void AtualizarTelefone();

        void AtualizarPessoa();
    }
}
