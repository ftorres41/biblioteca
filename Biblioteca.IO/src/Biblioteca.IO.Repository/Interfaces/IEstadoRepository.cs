﻿using Biblioteca.IO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IEstadoRepository : IRepository<Estado>
    {
        Estado ObterPorNome(string nome);
        IList<Estado> ObterTodos();
    }
}
