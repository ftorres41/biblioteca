﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IMaterialRepository : IRepository<Material>
    {
        void AtribuirAssunto();

        void AtribuirEditora();

        List<Material> ObterPorTitulo(string titulo);

        List<Material> ObterPorEditora(string nomeEditora);
    }
}
