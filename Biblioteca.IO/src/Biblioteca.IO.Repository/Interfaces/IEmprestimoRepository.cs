﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IEmprestimoRepository : IRepository<Emprestimo>
    {
        void RealizarEmprestimo(Usuario usuario, List<Material> materiais);

        void RealizarDevolucao(Usuario usuario, List<Material> materiais);

        List<Emprestimo> ObterPorUsuario(int id);

        List<Emprestimo> ObterPorDataPrevistaRetorno(DateTime dataPrevista);

        List<Emprestimo> ObterEmprestimosAtrasados(DateTime dataPrevista);
    }
}
