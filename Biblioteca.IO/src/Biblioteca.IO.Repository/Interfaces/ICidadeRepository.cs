﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface ICidadeRepository : IRepository<Cidade>
    {
        Cidade ObterPorId(int id);

        Cidade ObterPorNome(string nome);

        IList<Cidade> ObterPorEstado(int idEstado);

        IList<Cidade> ObterTodos();
    }
}
