﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Repository.Interfaces
{
    public interface IEnderecoRepository : IRepository<Endereco>
    {
        Endereco ObterPorUsuario(int idUsuario);
        void Atualizar(Endereco endereco);
        int Cadastrar(Endereco endereco);
    }
}
