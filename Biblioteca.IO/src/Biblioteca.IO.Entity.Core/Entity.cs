﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Entities.Core
{
    public abstract class Entity<T> : AbstractValidator<T> where T : Entity<T>
    {
        #region Propriedades

        public int Id { get; protected set; }
        public DateTime DataCadastro { get; protected set; }

        public ValidationResult ValidationResult { get; protected set; }

        #endregion

        #region Construtor
        protected Entity()
        {
            ValidationResult = new ValidationResult();
        }
        #endregion

        #region Métodos

        public abstract bool Valido();

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
                return true;

            if (ReferenceEquals(null, obj))
                return false;

            var entity = obj as Entity<T>;

            return entity != null && entity.Id == Id;
        }

        public override int GetHashCode()
        {
            return Tuple.Create(Id).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Tipo : {0} - [ID: {1}]", this.GetType().Name, Id);
        }

        #endregion
    }
}