﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.CrossCutting.Exceptions
{
    public class UsuarioNaoEncontradoException : Exception
    {
        public UsuarioNaoEncontradoException() : this("O usuário não foi encontrado")
        {

        }

        public UsuarioNaoEncontradoException(string message) : base(message)
        {

        }

        public UsuarioNaoEncontradoException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
