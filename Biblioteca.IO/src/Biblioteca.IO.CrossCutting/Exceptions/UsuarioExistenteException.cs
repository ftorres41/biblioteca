﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.CrossCutting.Exceptions
{
    public class UsuarioExistenteException : Exception
    {
        public UsuarioExistenteException()
        {

        }

        public UsuarioExistenteException(string message) : base(message)
        {

        }

        public UsuarioExistenteException(string message, Exception innerException) : base(message, innerException)
        {

        }
    }
}
