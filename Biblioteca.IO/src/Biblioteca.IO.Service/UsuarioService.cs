﻿using Biblioteca.IO.CrossCutting.Exceptions;
using Biblioteca.IO.CrossCutting.Helpers;
using Biblioteca.IO.Repository;
using Biblioteca.IO.Repository.Interfaces;
using Biblioteca.IO.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Service
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioService(IUsuarioRepository usuarioRepository)
        {
            if (usuarioRepository == null)
            {
                throw new ArgumentNullException(usuarioRepository.ToString());
            }

            _usuarioRepository = usuarioRepository;
        }

        public void CadastrarUsuario(Usuario usuario)
        {
            var usuarioExistente = _usuarioRepository.VerificarUsuarioExistente(usuario.Email);

            usuario.AtribuirSenha(usuario.Senha.ToHash());

            if (!usuario.Valido())
            {
                throw new UsuarioInvalidoException();
            }

            if (usuarioExistente)
            {
                throw new UsuarioExistenteException();
            }

            _usuarioRepository.CadastrarUsuario(usuario);
        }
    }
}
