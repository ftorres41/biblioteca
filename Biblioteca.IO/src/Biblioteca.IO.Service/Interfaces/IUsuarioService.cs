﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Service.Interfaces
{
    public interface IUsuarioService
    {
        void CadastrarUsuario(Usuario usuario);
    }
}
