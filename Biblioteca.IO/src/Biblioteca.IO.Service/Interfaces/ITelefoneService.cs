﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO.Service.Interfaces
{
    public interface ITelefoneService
    {
        void AtribuirTipoTelefone(TipoTelefone tipo);
    }
}
