﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Reserva : Entity<Reserva>
    {
        public DateTime DataReserva { get; private set; }
        public DateTime DataValidade { get; private set; }
        public List<Material> Materiais { get; private set; }
        public Usuario Usuario { get; private set; }

        public void RealizarReserva(List<Material> materiais, Usuario usuario)
        {

        }

        public void CancelarReserva(List<Material> materiais, Usuario usuario)
        {
            
        }

        public List<Reserva> ObterPorUsuario(int id)
        {
            return null;
        }

        public List<Reserva> ObterPorMaterial(string titulo)
        {
            return null;
        }

        public List<Reserva> ObterPorPeriodo(DateTime dataInicio, DateTime dataFim)
        {
            return null;
        }
    }
}
