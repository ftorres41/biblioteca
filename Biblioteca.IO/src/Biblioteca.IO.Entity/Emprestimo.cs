﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Emprestimo
    {
        public DateTime DataEmprestimo { get; private set; }
        public DateTime DataPrevistaRetorno { get; private set; }
        public DateTime DataRetorno { get; private set; }
        public List<Material> Materiais { get; private set; }
        public Usuario Usuario { get; private set; }

        public void RealizarEmprestimo(Usuario usuario, List<Material> materiais)
        {

        }

        public void RealizarDevolucao(Usuario usuario, List<Material> materiais)
        {

        }

        public List<Emprestimo> ObterPorUsuario(int id)
        {
            return null;
        }

        public List<Emprestimo> ObterPorDataPrevistaRetorno(DateTime dataPrevista)
        {
            return null;
        }

        public List<Emprestimo> ObterEmprestimosAtrasados(DateTime dataPrevista)
        {
            return null;
        }
    }
}
