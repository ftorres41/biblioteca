﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Artigo : Entity<Artigo>
    {
        public string Titulo { get; private set; }
        public List<Autor> Autores { get; private set; }

        public List<Artigo> ObterPorRevista(string colecao)
        {
            return null;
        }

        public List<Artigo> ObterPorAutor(string autor)
        {
            return null;
        }
    }
}
