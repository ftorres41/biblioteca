﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Material : Entity<Material>
    {
        public string Titulo { get; private set; }
        public Assunto Assunto { get; private set; }
        public Editora Editora { get; private set; }

        public void AtribuirAssunto()
        {

        }

        public void AtribuirEditora()
        {

        }

        public List<Material> ObterPorTitulo(string titulo)
        {
            return null;
        }

        public List<Material> ObterPorEditora(string nomeEditora)
        {
            return null;
        }
    }
}
