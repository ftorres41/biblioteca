﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Usuario : Entity<Usuario>
    {
        public string Email { get; private set; }
        public string Senha { get; private set; }
        public Pessoa Pessoa { get; private set; }

        public void AtribuirPessoa()
        {

        }
        
        public Usuario ObterPorEmail(string email)
        {
            return null;
        }

        public bool VerificarUsuarioExistente()
        {
            return false;
        }

        public bool VerificarUsuarioDesativado()
        {
            return false;
        }

        public void AlterarSenha()
        {

        }

        public void DesativarUsuario()
        {

        }

        public void CadastrarUsuario()
        {

        }
    }
}
