﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Assunto : Entity<Assunto>
    {
        public string Nome { get; private set; }
    }
}
