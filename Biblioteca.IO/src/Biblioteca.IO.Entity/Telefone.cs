﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Telefone : Entity<Telefone>
    {
        public int IdPessoa { get; private set; }
        public string Numero { get; private set; }
        public TipoTelefone TipoTelefone { get; private set; }

        public List<Telefone> ObterPorPessoa(int id)
        {
            return null;
        }

        public void AtribuirTipoTelefone()
        {

        }
    }
}
