﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Livro : Material
    {
        public string Edicao { get; private set; }
        public string Isbn { get; private set; }
        public List<Autor> Autores { get; private set; }

        public Livro ObterPorIsbn(string isbn)
        {
            return null;
        }

        public List<Livro> ObterPorAutor(string autor)
        {
            return null;
        }

        public void CadastrarLivro()
        {

        }

        public void AlterarLivro(string nome)
        {

        }
    }
}
