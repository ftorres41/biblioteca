﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Cidade : Entity<Cidade>
    {
        public string Nome { get; private set; }
        public Estado Estado { get; private set; }

        public void AtribuirEstado()
        {

        }
    }
}
