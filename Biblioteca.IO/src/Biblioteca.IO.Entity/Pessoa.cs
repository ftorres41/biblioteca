﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Pessoa : Entity<Pessoa>
    {
        public string Nome { get; private set; }
        public string Rg { get; private set; }
        public string Cpf { get; private set; }
        public List<Telefone> Telefones { get; private set; }
        public DateTime DataNascimento { get; private set; }
        public Endereco Endereco { get; private set; }

        public void CadastrarPessoa()
        {

        }

        public void AtribuirEndereco()
        {

        }

        public void AtribuirTelefones()
        {

        }

        public void AtualizarEndereco()
        {

        }

        public void AtualizarTelefone()
        {

        }

        public void AtualizarPessoa()
        {

        }
    }
}
