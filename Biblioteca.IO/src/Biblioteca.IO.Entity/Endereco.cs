﻿using Biblioteca.IO.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.IO
{
    public class Endereco : Entity<Endereco>
    {
        public string Logradouro { get; private set; }
        public string Bairro { get; private set; }
        public string Cep { get; private set; }
        public int Numero { get; private set; }
        public string Complemento { get; private set; }
        public Cidade Cidade { get; private set; }

        public void AtribuirCidade()
        {

        }
    }
}
